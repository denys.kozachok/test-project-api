package com.reqres.module.api.response.getuser;

import lombok.Data;

@Data
public class GetSingleUserResponseSupport {
    private String url;
    private String text;
}
