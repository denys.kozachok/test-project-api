package com.reqres.module.api.response.getuser;


import lombok.Data;

@Data
public class GetSingleUserResponse {
    private GetSingleUserResponseData data;
    private GetSingleUserResponseSupport support;
}
