package com.reqres.module.api.request.updateuser;

import lombok.Data;

@Data
public class UpdateUserRequest {
    private String name;
    private String job;

}
