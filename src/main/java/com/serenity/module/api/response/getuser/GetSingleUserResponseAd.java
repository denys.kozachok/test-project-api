package com.serenity.module.api.response.getuser;

import lombok.Data;

@Data
public class GetSingleUserResponseAd {
    private String url;
    private String text;
}
