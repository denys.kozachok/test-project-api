@CreateUserFeature @Users @API @Regression
Feature: Create User

  @Positive
  Scenario: Create user success
    Given [api] set name to 'Ivan'
    And   [api] set job to 'Quality Engineer'
    When  [api] send create user request
    Then  [api] create user status code must be '201'
    And   [api] create user response equals with request

  @Negative
  Scenario: Try to create user with empty name
    Given [api] set name to ''
    And   [api] set job to 'Quality Engineer'
    When  [api] send create user request
    Then  [api] create user status code must be '201'
#    to do when bug will be fixer uncomment next line
#    Then  [api] create user status code must be '400'

  @Negative
  Scenario: Try to create user with empty job
    Given [api] set name to 'Vlad'
    And   [api] set job to ''
    When  [api] send create user request
    Then  [api] create user status code must be '201'
#    to do when bug will be fixer uncomment next line
#    Then  [api] create user status code must be '400'

  @Negative
  Scenario: Try to create user with empty job and name
    Given [api] set name to ''
    And   [api] set job to ''
    When  [api] send create user request
    Then  [api] create user status code must be '201'
    #    to do when bug will be fixer uncomment next line
#    Then  [api] create user status code must be '400'