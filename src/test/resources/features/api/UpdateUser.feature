@UpdateUserFeature @Users @API @Regression
Feature: Update User

  @Positive
  Scenario: Update user success
    Given [api] set id to '1'
    And   [api] set name to 'Widyan'
    And   [api] set job to 'SDET'
    When  [api] send update user request
    Then  [api] update user status code must be '200'
    And   [api] update user response equals with request

  @Negative
  Scenario: Update user with empty job
    Given [api] set id to '1'
    And   [api] set name to 'Fridir'
    And   [api] set job to ''
    When  [api] send update user request
    Then  [api] update user status code must be '400'

  @Negative
  Scenario: Update user with empty id
    Given   [api] set name to 'Fridir'
    And   [api] set job to 'AQA'
    When  [api] send update user request
    Then  [api] update user status code must be '404'

  @Negative
  Scenario: Update user with empty name
    Given [api] set id to '2'
    And   [api] set name to ''
    And   [api] set job to 'AQA'
    When  [api] send update user request
    Then  [api] update user status code must be '404'

  @Negative
  Scenario: Update user with empty body
    Given [api] set name to ''
    And   [api] set job to ''
    When  [api] send update user request
    Then  [api] update user status code must be '404'
